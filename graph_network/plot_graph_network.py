import bpy
import bmesh
import torch
import numpy as np
from mathutils import Vector

##

def get_custom_material():
    # Create a new material
    material = bpy.data.materials.new(name="Custom_Material")
    # Enable 'Use Nodes'
    material.use_nodes = True
    # Enable good rendering
    material.blend_method = 'BLEND'  # enable alpha transparency
    material.shadow_method = 'HASHED'  # This allows for alpha shadows
    material.diffuse_color = (1, 0, 0, 1) # Base color red
    # Get the material node tree
    nodes = material.node_tree.nodes
    # Clear all nodes to start clean
    for node in nodes:
        nodes.remove(node)
    # Add Principled BSDF node
    principled_node = nodes.new(type='ShaderNodeBsdfPrincipled')
    principled_node.location = 0,0
    principled_node.inputs['Base Color'].default_value = (1, 0, 0, 1) # Base color red
    # Add Transparent BSDF node
    transparent_node = nodes.new(type='ShaderNodeBsdfTransparent')
    transparent_node.location = 0,200
    # Add Mix Shader node
    mix_node = nodes.new(type='ShaderNodeMixShader')
    mix_node.location = 200,100
    mix_node.inputs[0].default_value = 0.5  # Set the mix ratio to 0.5
    # Add Material Output node
    output_node = nodes.new(type='ShaderNodeOutputMaterial')
    output_node.location = 400,100
    # Connect nodes
    links = material.node_tree.links
    link = links.new(mix_node.inputs[1], principled_node.outputs[0])  # Connect Principled to Mix Shader
    link = links.new(mix_node.inputs[2], transparent_node.outputs[0])  # Connect Transparent to Mix Shader
    link = links.new(output_node.inputs[0], mix_node.outputs[0])  # Connect Mix Shader to Material Output
    return material


def set_alpha_keyframe(material, alpha_value, frame_number):
    # Get the material node tree
    nodes = material.node_tree.nodes
    # Locate the Principled BSDF node and set its alpha value
    for node in nodes:
        if node.type == 'BSDF_PRINCIPLED':
            node.inputs['Alpha'].default_value = alpha_value
            # Set the keyframe at the specified frame number
            node.inputs['Alpha'].keyframe_insert(data_path="default_value", frame=frame_number)


def set_base_color_keyframe(material, color_value, frame_number):
    # Get the material node tree
    nodes = material.node_tree.nodes
    # Locate the Principled BSDF node and set its base color value
    for node in nodes:
        if node.type == 'BSDF_PRINCIPLED':
            node.inputs['Base Color'].default_value = color_value
            # Set the keyframe at the specified frame number
            node.inputs['Base Color'].keyframe_insert(data_path="default_value", frame=frame_number)


##

def init_blender_scene():
    """
    Initialize the Blender scene. Set the scene dimensions, lighting, camera angles, and any other general scene properties. 
    """
    # Clear all mesh objects
    #bpy.ops.object.select_all(action='DESELECT')
    #bpy.ops.object.select_by_type(type='MESH')
    #bpy.ops.object.delete()
    # Set scene dimensions
    bpy.context.scene.render.resolution_x = 1920
    bpy.context.scene.render.resolution_y = 1080
    # Set up lighting 
    bpy.ops.object.light_add(type='AREA', location=(0, 0, 3))  # Add an Area light at the center top
    light = bpy.context.object
    light.data.size = 1  # Increase the size for soft light
    light.data.energy = 5000  # Increase the intensity
    #print(light.data.color)
    light.data.color = (0.8, 0.7, 1.0)  # Slightly bluish light for an ethereal feel
    # Set up camera
    bpy.ops.object.camera_add(location=(1.6, -1.6, 0.72))  # Change these values as per your need
    camera = bpy.context.object
    ## in degrees it's supposed to be
    camera.rotation_euler = (1.186823844909668, 0.0, 0.7853981852531433)  # Rotate the camera to face the center
    camera.data.lens = 35  # 35mm lens, change this as per your need
    camera.data.dof.use_dof = True  # Enable depth of field
    camera.data.dof.aperture_fstop = 2.8  # Smaller value for wider aperture and more blur
    bpy.context.scene.camera = camera  # Set the active camera
    # Set background color
    bpy.context.scene.world.color = (0.8, 0.9, 1.0)  # Light blue background
    bpy.ops.object.light_add(type='POINT', location=(0, 0, 5))
    #bpy.context.scene.eevee.use_shadow = True
    bpy.context.scene.eevee.shadow_cube_size = '1024'
    bpy.context.scene.eevee.shadow_cascade_size = '1024'
    ## Make the world dark
    # Clear all nodes in the world shading node tree
    bpy.context.scene.world.use_nodes = True
    world_nodes = bpy.context.scene.world.node_tree.nodes
    for node in world_nodes:
        world_nodes.remove(node)
    # Create a new background node
    background_node = world_nodes.new(type='ShaderNodeBackground')
    background_node.inputs[0].default_value = (0, 0, 0, 1)  # Set color to black
    # Create a new output node
    output_node = world_nodes.new('ShaderNodeOutputWorld')
    # Link the nodes
    bpy.context.scene.world.node_tree.links.new(background_node.outputs[0], output_node.inputs[0])



def get_unique_edges(adj_list):
    """
    Process adjacency list into a set of unique edges.
    """
    unique_edges = set()
    for frame_adj in adj_list:
        for i, row in enumerate(frame_adj):
            for j in row[1:]:
                edge = "_".join(sorted([str(int(i)), str(int(j.item()))]))  # convert tensor to int, then to string
                unique_edges.add(edge)
    return list(unique_edges)




def initialize_nodes(pos):
    """
    Initialize nodes using the given position list.
    """
    nodes = []
    # Create an ico_sphere object to instance
    bpy.ops.mesh.primitive_ico_sphere_add(location=(0, 0, 0))  # location doesn't matter here
    base_ico_sphere = bpy.context.active_object
    base_ico_sphere.scale = (0.001, 0.001, 0.001)  # make the sphere 1000 times smaller
    base_ico_sphere.hide_render = True  # hide in render
    base_ico_sphere.hide_viewport = True  # hide in the viewport
    for i, node_pos in enumerate(pos):
        # Create a new object with the same data
        node = bpy.data.objects.new(f'Node_{i}', base_ico_sphere.data)
        node.scale = (0.005, 0.005, 0.005)
        # Set location
        node.location = Vector(node_pos)
        # Link the object to the scene
        bpy.context.scene.collection.objects.link(node)
        nodes.append(node)
    return base_ico_sphere, nodes


def initialize_edges(unique_edges, nodes):
    """
    Initialize edges using the given unique edges list.
    """
    edges = []
    for edge in unique_edges:
        node_a, node_b = edge.split('_')
        # create curve data
        curve_data = bpy.data.curves.new(name=f'Curve_{edge}', type='CURVE')
        curve_data.dimensions = '3D'
        polyline = curve_data.splines.new('POLY')
        polyline.points.add(1)
        polyline.points[0].co = nodes[int(node_a)].location.to_4d()
        polyline.points[1].co = nodes[int(node_b)].location.to_4d()
        # create curve object
        curve_obj = bpy.data.objects.new(f'Edge_{edge}', curve_data)
        bpy.context.collection.objects.link(curve_obj)
        # create a simple material
        mat = get_custom_material()
        #mat = bpy.data.materials.new(name="Edge_Material")
        #mat.use_backface_culling = True  # Turn off the "Show Backface" option
        #mat.blend_method = 'BLEND'  # enable alpha transparency
        #mat.shadow_method = 'HASHED'  # This enables alpha hashing
        #mat.diffuse_color = (1, 0, 0, 0)  # set alpha to 0
        #mat.keyframe_insert(data_path="diffuse_color", frame=1)  # keyframe the color including alpha
        # assign it to the curve object
        curve_obj.data.materials.append(mat)
        curve_data.bevel_depth = 0.002  # adjust as needed
        edges.append(curve_obj)
    return edges


def connect_edges_to_nodes(edges, nodes):
    """
    Connect each edge to its nodes using hooks.
    """
    for edge in edges:
        node_a, node_b = edge.name.split('_')[1:]
        print(edge, node_a, node_b)
        # create hook modifiers
        bpy.context.view_layer.objects.active = edge
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.curve.select_all(action='DESELECT')  # deselect all curve vertices
        #
        # Select first vertex and hook to node_a
        edge.data.splines[0].points[0].select = True  # select first vertex
        bpy.ops.object.mode_set(mode='OBJECT')  # change to OBJECT mode
        print("node a:")
        print(nodes[int(node_a)])
        nodes[int(node_a)].select_set(True)  # select node_a
        bpy.context.view_layer.objects.active = edge  # set edge as the active object
        bpy.ops.object.mode_set(mode='EDIT')  # change back to EDIT mode
        bpy.ops.object.hook_add_selob()
        #
        bpy.ops.curve.select_all(action='DESELECT')  # deselect all curve vertices
        #
        # Select second vertex and hook to node_b
        edge.data.splines[0].points[1].select = True  # select second vertex
        bpy.ops.object.mode_set(mode='OBJECT')  # change to OBJECT mode
        print("node b:")
        print(nodes[int(node_b)])
        nodes[int(node_b)].select_set(True)  # select node_b
        bpy.context.view_layer.objects.active = edge  # set edge as the active object
        bpy.ops.object.mode_set(mode='EDIT')  # change back to EDIT mode
        bpy.ops.object.hook_add_selob()
        #
        bpy.ops.object.mode_set(mode='OBJECT')  # change back to OBJECT mode


def connect_edges_to_nodes(edges, nodes):
    """
    Connect each edge to its nodes using hooks.
    """
    for edge in edges:
        node_a, node_b = edge.name.split('_')[1:]
        print(edge, node_a, node_b)
        # Get objects from bpy.data.objects
        node_a_obj = bpy.data.objects[f"Node_{node_a}"]
        node_b_obj = bpy.data.objects[f"Node_{node_b}"]
        # Add hook modifier for node_a
        hook_a = edge.modifiers.new(f"Hook_Node_{node_a}", 'HOOK')
        hook_a.object = node_a_obj
        hook_a.vertex_indices_set([0])  # Assuming the vertex index for node_a is 0
        # Add hook modifier for node_b
        hook_b = edge.modifiers.new(f"Hook_Node_{node_b}", 'HOOK')
        hook_b.object = node_b_obj
        hook_b.vertex_indices_set([1])  # Assuming the vertex index for node_b is 1



def initialize_nodes_and_edges(adj, init_pos):
    import time
    print("getting all edges")
    unique_edges = get_unique_edges(adj)
    print("    found",len(unique_edges),"edges")
    print("initializing nodes")
    ni_start = time.time()
    og_node, nodes = initialize_nodes(init_pos)
    ni_end = time.time()
    print("initializing edges")
    ei_start = time.time()
    edges = initialize_edges(unique_edges, nodes)
    ei_end = time.time()
    connect_edges_to_nodes(edges, nodes)
    ci_end = time.time()
    print("node initialization:",ni_end-ni_start)
    print("edge initialization:",ei_end-ei_start)
    print("edge connection:",ci_end-ei_end)
    return og_node, nodes, edges


def animate_nodes_and_edges(nodes, edges, pos, adj_list, mask):
    """
    Iterate through each frame and animate nodes and edges. At the start of each frame,
    we assume all edges are invisible, and then make visible those edges that are in the current frame's adjacency list.
    """
    for frame_idx, (frame_pos, frame_adj, frame_mask) in enumerate(zip(pos, adj_list, mask)):
        print("\n\n\nframe:",frame_idx)
        current_frame = 10+frame_idx*10
        bpy.context.scene.frame_set(current_frame)
        # Set all edges to invisible
        for edge in edges:
            mat = edge.data.materials[0]  # assuming the material is at index 0
            set_alpha_keyframe(mat, 0, current_frame)
            #############
            #mat.diffuse_color = (0, 0, 0, 0)  # red color with 0 alpha
            #mat.keyframe_insert(data_path="diffuse_color", frame=current_frame)  # keyframe the color including alpha
            #############
        # Move nodes and make corresponding edges visible
        for i, node_pos in enumerate(frame_pos):
            nodes[i].location = tuple(node_pos.tolist()) # Converting tensor to list then to tuple
            nodes[i].keyframe_insert(data_path="location")
            print(i, frame_adj[i])
            temp_adj = frame_adj[i][1:]  # Exclude the node itself
            temp_mask = frame_mask[i][1:]  # Corresponding mask entries
            for adj, mask in zip(temp_adj, temp_mask):
                if mask:  # If the edge is supposed to be visible
                    edge_name = "_".join(sorted([str(int(i)), str(int(adj.item()))]))  # convert tensor to int, then to string
                    print(edge_name)
                    # If the edge is supposed to be visible
                    edge = bpy.data.objects[f'Edge_{edge_name}']
                    mat = edge.data.materials[0]  # assuming the material is at index 0
                    #############
                    set_alpha_keyframe(mat, 1, current_frame)
                    #mat.use_backface_culling = True  # Turn off the "Show Backface" option
                    #mat.blend_method = 'BLEND'  # enable alpha transparency
                    #mat.shadow_method = 'HASHED'  # This disable shadows entirely
                    #mat.diffuse_color = (1, 0, 0, 1)  # red color with 1 alpha
                    #mat.keyframe_insert(data_path="diffuse_color", frame=current_frame)  # keyframe the color including alpha
                    #############


def group_keyframes_by_frame(objects):
    """
    Groups all keyframes of a list of objects by frame number.

    Args:
        objects (list[bpy.types.Object]): The objects whose keyframes to group.

    Returns:
        dict: A dictionary where each key is a frame number and each value is a list
            of dictionaries representing the keyframes at that frame.
            Each keyframe dictionary has 'object', 'data_path', 'array_index', and 'value'.
    """
    grouped_keyframes = {}
    for obj in objects:
        if obj.animation_data and obj.animation_data.action:
            for fcurve in obj.animation_data.action.fcurves:
                for keyframe in fcurve.keyframe_points:
                    frame = keyframe.co[0]  # The frame number is the x-coordinate
                    frame = round(frame)  # Ensure it's an integer, in case of floating point inaccuracies
                    data_path = fcurve.data_path
                    array_index = fcurve.array_index
                    value = keyframe.co[1]  # The value is the y-coordinate
                    keyframe_info = {
                        'object': obj,
                        'data_path': data_path,
                        'array_index': array_index,
                        'value': value,
                    }
                    if frame not in grouped_keyframes:
                        grouped_keyframes[frame] = []
                    grouped_keyframes[frame].append(keyframe_info)
    return grouped_keyframes

def shift_keyframes(grouped_keyframes, shift):
    """
    Shift all keyframes by a given number of frames.

    Args:
        grouped_keyframes (dict): A dictionary of keyframes grouped by frame number,
            as returned by group_keyframes_by_frame.
        shift (int): The number of frames to shift the keyframes by.
    """
    for frame in sorted(grouped_keyframes.keys(), reverse=(shift > 0)):
        new_frame = frame + shift
        for keyframe_info in grouped_keyframes[frame]:
            obj = keyframe_info['object']
            data_path = keyframe_info['data_path']
            array_index = keyframe_info['array_index']
            value = keyframe_info['value']
            # Delete the original keyframe
            obj.keyframe_delete(data_path, frame=frame, index=array_index)
            # Set the new keyframe
            setattr(obj, data_path, value)  # Set the property
            obj.keyframe_insert(data_path, frame=new_frame, index=array_index)  # Insert the keyframe



def convert_pos_dict_to_array(in_dict):
    array_len = max(list(in_dict.keys()))+1
    print(min(list(in_dict.keys())))
    out_array = np.zeros((int(array_len),in_dict[array_len-1].shape[0]))
    print(out_array.shape)
    for key, loc in in_dict.items():
        #print(key, loc)
        out_array[int(key)]=loc
    return(out_array)


def get_camera():
    camera_object = None
    for obj in bpy.data.objects:
        if obj.type == 'CAMERA':
            camera_object = obj
            break
    return(camera_object)


def delete_object_keyframes(obj_list):
    # Iterate over all the object's animation data properties
    for obj in obj_list:
        if obj.animation_data:
            for fcurve in obj.animation_data.action.fcurves:
                # Iterate over all keyframes in this property
                for keyframe in fcurve.keyframe_points:
                    # Delete this keyframe
                    fcurve.keyframe_points.remove(keyframe)



def main(adj, dists, mask, pos):
    init_blender_scene()
    og_node, nodes, edges = initialize_nodes_and_edges(adj, pos[0])  # Initialize with position data from the first frame
    animate_nodes_and_edges(nodes, edges, pos, adj, mask)


#########################################################################



########################
# Test data
adj = [torch.tensor(
[[0,1,2],
 [1,2,3],
 [2,3,4],
 [3,4,5],
 [4,5,6],
 [5,6,1],
 [6,1,2]],
dtype=torch.long
),
torch.tensor([[0,1,2],
 [1,2,3],
 [2,3,4],
 [3,4,5],
 [4,5,6],
 [5,6,1],
 [6,1,2]], dtype=torch.long
)
]
dists = [torch.tensor(
[[0,1,2],
 [0,1,2],
 [0,1,2],
 [0,1,2],
 [0,1,2],
 [0,1,2],
 [0, 1, 2]], dtype=torch.float),
torch.tensor(
[[0,10,20],
 [0,10,20],
 [0,10,20],
 [0,10,20],
 [0,10,20],
 [0,10,20],
 [0,10,20]], dtype=torch.float)]
mask = [torch.tensor([[False, True, True]] * 7 , dtype=torch.bool), torch.tensor([[False, True, False]] * 7 , dtype=torch.bool)]

n = adj[0].shape[0]  # the number of nodes in your graph

# Create coordinates for points on a circle
theta = np.linspace(0, 2*np.pi, n+1)[:-1]  # angles evenly distributed around the circle
r = 10  # radius of the circle

x = r * np.cos(theta)  # x coordinates
y = r * np.sin(theta)  # y coordinates
z = np.zeros(n)  # z coordinates (all zero to create a 2D circle)

pos_1 = torch.tensor(np.column_stack((x, y, z)), dtype=torch.float)

# To create the second keyframe, we can rotate the nodes by applying a rotation matrix
angle = np.pi / 6  # rotation angle (e.g., 30 degrees)

rotation_matrix = np.array([[np.cos(angle), -np.sin(angle), 0], 
                            [np.sin(angle), np.cos(angle), 0],
                            [0, 0, 1]])

pos_2 = torch.tensor(np.dot(rotation_matrix, np.column_stack((x, y, z)).T).T, dtype=torch.float)

pos = [pos_1, pos_2]

########################
import pickle
import bz2
def compressed_pkl_dump(in_obj, out_file):
    ofile = bz2.BZ2File(out_file,'wb')
    pickle.dump(in_obj,ofile)
    ofile.close()
    return


def compressed_pkl_load(in_file):
    ifile = bz2.BZ2File(in_file,'rb')
    newdata = pickle.load(ifile)
    ifile.close()
    return newdata


def convert_pos_dict_to_array(in_dict):
    array_len = max(list(in_dict.keys()))+1
    print(min(list(in_dict.keys())))
    out_array = np.zeros((int(array_len),in_dict[array_len-1].shape[0]))
    print(out_array.shape)
    for key, loc in in_dict.items():
        out_array[int(key)]=loc
    return(out_array)



results_dict = compressed_pkl_load("/home/scott/Downloads/filtered_gene_bc_matrices/result_dict_3D.pkl")
result_key = 'k_20_thresh_1_frac_0.25'

starting_adj, starting_pos = compressed_pkl_load("/home/scott/Downloads/filtered_gene_bc_matrices/starting_Adj_pos_3D.pkl")

# First we rotate the positions
adj =[ starting_adj, starting_adj ]
mask=[ np.zeros_like(starting_adj,dtype=bool), np.ones_like(starting_adj,dtype=bool) ]# originally start with all edges
pos =[ starting_pos, starting_pos ] # Then stay there while pruning
     # TODO: Then add the second neighbors (so change the next mask and adj)
     #       Then update positions to show rotation


def add_rotation_and_prune_step(adj, mask, pos, new_adj, new_mask, new_pos):
    if type(new_pos)==dict:
        new_pos = convert_pos_dict_to_array(new_pos)
    # first do the rotation (updat pos, but hold the rest)
    adj.append(adj[-1])
    mask.append(mask[-1])
    pos.append(new_pos)
    # Then prune, so update adj and mask, but hold pos
    adj.append(new_adj)
    mask.append(new_mask)
    pos.append(pos[-1])
    # 
    #adj.append(adj[-1])
    #mask.append(mask[-1])
    #pos.append(new_pos)
    return(adj, mask, pos)


adj, mask, pos = add_rotation_and_prune_step(
    adj, mask, pos, 
    results_dict[result_key]['knn_adj_list'], 
    results_dict[result_key]['knn_mask'], 
    results_dict[result_key]['end_positions']
)



########################

# Now call the main function with your data
#main(adj, dists, mask, pos)



# Initialize with position data from the first frame
init_blender_scene()
og_node, nodes, edges = initialize_nodes_and_edges(adj, pos[0])  # Initialize with position data from the first frame
animate_nodes_and_edges(nodes, edges, pos, adj, mask)
#all_keyframes = group_keyframes_by_frame(nodes+edges)

